from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

origins = ["http://localhost:8080"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET"],
    allow_headers=["*"],
)


@app.get('/', response_model=str)
async def login() -> str:
    return f"Hello world !"


@app.get('/hello/{name}', response_model=str)
async def login(name: str) -> str:
    return f"Hello {name} !"
