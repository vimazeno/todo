# todo

a todo app to learn and discuss architecture api + web ui + cli implemented with Python fast API, Python click, vuejs

* https://vuejs.org/
* https://click.palletsprojects.com/en/8.0.x/

## API

The API is build using [fast-api](https://fastapi.tiangolo.com/) framework. \

### Package management

As a package manager we used [poetry](https://python-poetry.org) \
To install dependencies, change directory to ./api/ then run `poetry install`. \
Tu run a command using poetry use `petry run <your-command>`.