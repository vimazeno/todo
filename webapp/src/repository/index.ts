import { apiUrl } from "@/env";

const repository = {
  getHelloName: (name: string) => {
    console.log(`http://${apiUrl}/hello/${name}`);
    return fetch(`http://${apiUrl}/hello/${name}`, {
      method: "GET",
      headers: {
        "Access-Control-Allow-Origin": apiUrl,
      },
    }).then((response) => {
      if (response.ok) {
        return response.text();
      }
      throw new Error(response.statusText);
    });
  },
};

export default repository;
